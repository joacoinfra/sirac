#!/bin/bash

apt update && apt install apache2 git -y

deploy_app () {
    cd /sirac && git pull
}

if ! test -d /sirac; then

    git clone https://gitlab.com/joacoinfra/sirac.git /sirac

    deploy_app
else
    deploy_app
fi

exec "$@"
